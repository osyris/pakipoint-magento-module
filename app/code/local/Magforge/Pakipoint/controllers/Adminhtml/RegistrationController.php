<?php

class Magforge_Pakipoint_Adminhtml_RegistrationController extends Mage_Adminhtml_Controller_Action
{
	/**
	 * Show Registration Form
	*/
	public function indexAction()
	{
		$this->_title($this->__('Pakipoint Account Registration'));
		$this->loadLayout();
		
		$registration = $this->getLayout()->createBlock('pakipoint/adminhtml_registration_view', 'registration_container');
		$this->getLayout()->getBlock('content')->append($registration);
		
		$this->renderLayout();
	}
	
	
	/**
	 * Process Registration POST
	*/
	public function saveAction()
	{
		$params = $this->getRequest()->getParams();
		
		// save posted data to session
		$this->getSess()->setData('pakipoint_registration_data', $params);
		
		$helper = Mage::helper('pakipoint');
		$mode = Mage::getStoreConfig('carriers/pakipoint/api_endpoint_type');
		
		// register account with Pakipoint
		$client = Mage::getModel('pakipoint/api_client');
		$regResult = $client->registerAccount($params);		
		$helper->log($regResult);

		if($client->getError()){
			$errors = (array) json_decode($client->getError());
			foreach($errors as $group => $fields){
				$errorMsgs = $helper->getFormattedRegistrationErrorMessages($group, $fields);
				foreach($errorMsgs as $errorMsg){
					$this->getSess()->addError($errorMsg);
				}
			}
			$this->_redirectReferer();
			return;
		}
		
		if(
			!isset($regResult->account->code) 
			|| $regResult->account->code == ''
			|| !isset($regResult->token)
			|| $regResult->token == ''
		){
			$this->getSess()->addError($helper->__('Registration result is missing merchant code or token.'));
			$this->_redirectReferer();
			return;
		}
		
		// save merchant code and access token in module config
		try{
			$cnfKey = 'carriers/pakipoint/'.$mode;
			Mage::getConfig()->saveConfig($cnfKey.'_merchant_code', $regResult->account->code, 'default', 0);
			Mage::getConfig()->saveConfig($cnfKey.'_auth_token', $regResult->token, 'default', 0);
			$this->getSess()->addSuccess($helper->__('Successfully registered Pakipoint account and configured Module.'));

		}catch(Exception $e){
			$this->getSess()->addError($helper->__('Failed saving registration data in Magento. %s', $e->getMessage()));
			$this->_redirectReferer();
			return;
		}
		
		// create SOAP API user and permissions
		$apiUser		= 'pakipoint';
		$apiUserEmail	= 'info@pakipoint.ee';
		$roleName		= 'Pakipoint Integration';
		$apiKey			= Mage::helper('core')->getRandomString(20);
		$apiUrl			= Mage::getUrl('', array(
			'_direct' => 'api/v2_soap?wsdl=1',
			'_nosid' => true,
			'_use_rewrite' => false,
			'_store' => 0
		));
		
		try{
			$role = Mage::getModel('api/roles')->load($roleName, 'role_name');
			$role->setName($roleName)->setPid(0)->setRoleType('G')->save();	
			Mage::getModel('api/rules')->setRoleId($role->getId())->setResources(array('all'))->saveRel();
			
			$user = Mage::getModel('api/user')->load($apiUserEmail, 'email');
			$user->addData(array(
				'username' => $apiUser,
				'firstname' => 'Pakipoint',
				'lastname' => 'Integration',
				'email' => $apiUserEmail,
				'new_api_key' => $apiKey,
				'api_key_confirmation' => $apiKey,
				'is_active' => 1
			))->save();
			
			$user->setRoleIds(array($role->getId()))->setRoleUserId($user->getUserId())->saveRelations();
			
			$this->getSess()->addSuccess($helper->__('Created SOAP API User and Permissions in Magento'));
			
		}catch(Exception $e){
			$this->getSess()->addError($helper->__('Failed creating SOAP API User and Permissions in Magento. %s', $e->getMessage()));
			$this->_redirectReferer();
			return;
		}
		
		// send API user credentials to Pakipoint
		$params = array(
			'account_code' => $regResult->account->code,
			'service' => 'magento',
			'endpoint' => $apiUrl,
			'username' => $apiUser,
			'password' => $apiKey,
			'certification' => Mage::getStoreConfig('carriers/pakipoint/api_endpoint_type') == 'live' ? 'production' : 'development',
			'is_active' => true,
			'is_read_only' => false,
			'auto_sync' => true
		);
		
		$client = Mage::getModel('pakipoint/api_client');
		$client->setConfig($mode.'_auth_token', $regResult->token);
		$apiCredSendResult = $client->registerMagentoApiCredentials($params);
		$helper->log($apiCredSendResult);
		
		if($client->getError()){
			$this->getSess()->addError($helper->__('Failed sending SOAP API access credentials to Pakipoint. '.$client->getError()));
			$this->_redirectReferer();
			return;
		}
		
		$this->getSess()->addSuccess($helper->__('Registered SOAP API access credentials with Pakipoint'));
		
		// save config flag, so registration link is not shown any more
		Mage::getConfig()->saveConfig('carriers/pakipoint/registered_'.$mode, '1', 'default', 0);
		
		// remove data from session
		$this->getSess()->unsData('pakipoint_registration_data');
		
		// finally redirect to shipping options configuration page
		$this->_redirectUrl(Mage::helper('adminhtml')->getUrl('adminhtml/system_config/edit', array('section' => 'carriers')));
		
		return;
	}
	
	
	public function skipAction()
	{
		$mode = Mage::getStoreConfig('carriers/pakipoint/api_endpoint_type');
		Mage::getConfig()->saveConfig('carriers/pakipoint/registered_'.$mode, '1', 'default', 0);
		$url = Mage::helper('adminhtml')->getUrl('adminhtml/system_config/edit', array('section' => 'carriers'));
		
		$this->_redirectUrl($url);
	}
	
	
	public function pakipointloginAction()
	{
		$client = Mage::getModel('pakipoint/api_client');
		$result = $client->requestLoginLink();
		$helper = Mage::helper('pakipoint');
		
		if($client->getError()){
			$this->getSess()->addError($helper->__('Failed loging in to Pakipoint Admin. '.$client->getError()));
			$this->_redirectReferer();
			return;
		}
		
		if(!isset($result->link)){
			$this->getSess()->addError($helper->__('Woops! Failed loading login link.'));
			$this->_redirectReferer();
			return;
		}
		
		$this->_redirectUrl($result->link);
		return;
	}
	
	
	public function getSess()
	{
		return Mage::getSingleton('adminhtml/session');
	}
}