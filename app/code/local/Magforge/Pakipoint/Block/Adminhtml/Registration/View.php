<?php

class Magforge_Pakipoint_Block_Adminhtml_Registration_View extends Mage_Adminhtml_Block_Widget_Form_Container
{	
    protected function _prepareLayout()
    {
        $this->_objectId	= 'block_id';
        $this->_blockGroup	= 'pakipoint';
        $this->_controller	= 'adminhtml_registration';
        $this->_mode		= 'view';
        $mode = strtoupper(Mage::getStoreConfig('carriers/pakipoint/api_endpoint_type'));
        
		$this->_headerText = Mage::helper('pakipoint')->__('Pakipoint '.$mode.' Account Registration');
		
		parent::_prepareLayout();
		
		$this->removeButton('add');
		$this->removeButton('back');
		$this->removeButton('reset');
		
		$this->_updateButton('save', 'label', Mage::helper('pakipoint')->__('Register '.$mode.' Account'));
		
		$this->_addButton('delete', array(
			'label'		=> Mage::helper('adminhtml')->__('Skip Registration'),
			'class'		=> 'delete',
			'onclick'	=> 'deleteConfirm(\''
		        . Mage::helper('core')->jsQuoteEscape(
		            Mage::helper('pakipoint')->__('This will remove registration link and you will need to configure Pakipoint module manually. Are you sure you want to do this?')
		        )
		        .'\', \''
		        . Mage::helper('adminhtml')->getUrl('adminhtml/adminhtml_registration/skip')
		        . '\')',
		));
    }
}
