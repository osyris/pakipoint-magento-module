<?php

class Magforge_Pakipoint_Block_Adminhtml_Registration_View_Form extends Mage_Adminhtml_Block_Widget_Form
{
 
    protected function _prepareForm()
    {
		$form = new Varien_Data_Form(array('id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post'));
		
		$form->setHtmlIdPrefix('block_');
		
		// COMPANY FIELDSET
		$fieldsetCompany = $form->addFieldset('company_fieldset', array(
			'legend' => Mage::helper('pakipoint')->__('Company Information'),
			'class' => 'fieldset'
		));
		
		$fieldsetCompany->addField('account[name]', 'text', array(
			'name'      => 'account[name]',
			'label'     => Mage::helper('pakipoint')->__('Account Name'),
			'title'     => Mage::helper('pakipoint')->__('Account Name'),
			'required'  => true,
//			'class'     => 'validate-number',
			'value'		=> Mage::getStoreConfig('general/store_information/name'),//.' '.Mage::helper('core')->getRandomString(4, '1234567890')
		));
		
		$fieldsetCompany->addField('account[company]', 'text', array(
			'name'      => 'account[company]',
			'label'     => Mage::helper('pakipoint')->__('Company Name'),
			'title'     => Mage::helper('pakipoint')->__('Company Name'),
			'required'  => true,
//			'class'     => 'validate-number',
			'value'		=> Mage::getStoreConfig('general/store_information/name')
		));
		
		$fieldsetCompany->addField('account[registration_number]', 'text', array(
			'name'      => 'account[registration_number]',
			'label'     => Mage::helper('pakipoint')->__('Registration Number'),
			'title'     => Mage::helper('pakipoint')->__('Registration Number'),
			'required'  => false,
		));
		
		$fieldsetCompany->addField('account[vat_number]', 'text', array(
			'name'      => 'account[vat_number]',
			'label'     => Mage::helper('pakipoint')->__('VAT Number'),
			'title'     => Mage::helper('pakipoint')->__('VAT Number'),
			'required'  => false,
			'value'		=> Mage::getStoreConfig('general/store_information/merchant_vat_number')
		));
		
		$fieldsetCompany->addField('account[address]', 'text', array(
			'name'      => 'account[address]',
			'label'     => Mage::helper('pakipoint')->__('Registered Address'),
			'title'     => Mage::helper('pakipoint')->__('Registered Address'),
			'required'  => false,
			'value'		=> Mage::getStoreConfig('shipping/origin/street_line1')
		));
		
		$fieldsetCompany->addField('account[city]', 'text', array(
			'name'      => 'account[city]',
			'label'     => Mage::helper('pakipoint')->__('Registered City'),
			'title'     => Mage::helper('pakipoint')->__('Registered City'),
			'required'  => false,
			'value'		=> Mage::getStoreConfig('shipping/origin/city')
		));
		
		$fieldsetCompany->addField('account[postal_code]', 'text', array(
			'name'      => 'account[postal_code]',
			'label'     => Mage::helper('pakipoint')->__('Registered Zip/Postal Code'),
			'title'     => Mage::helper('pakipoint')->__('Registered Zip/Postal Code'),
			'required'  => false,
			'value'		=> Mage::getStoreConfig('shipping/origin/postcode')
		));
		
		$fieldsetCompany->addField('account[country]', 'select', array(
			'name'      => 'account[country]',
			'label'     => Mage::helper('pakipoint')->__('Registered Country'),
			'title'     => Mage::helper('pakipoint')->__('Registered Country'),
			'required'  => true,
			'values'	=> Mage::getModel('directory/country')->getResourceCollection()->loadByStore()->toOptionArray(false),
			'value'		=> Mage::getStoreConfig('shipping/origin/country_id')
		));
		
		$fieldsetCompany->addField('account[phone]', 'text', array(
			'name'      => 'account[phone]',
			'label'     => Mage::helper('pakipoint')->__('Company Phone Number'),
			'title'     => Mage::helper('pakipoint')->__('Company Phone Number'),
			'required'  => true,
			'value'		=> Mage::getStoreConfig('general/store_information/phone')
		));
		
		$fieldsetCompany->addField('account[email]', 'text', array(
			'name'      => 'account[email]',
			'label'     => Mage::helper('pakipoint')->__('Company Email Address'),
			'title'     => Mage::helper('pakipoint')->__('Company Email Address'),
			'required'  => true,
			'class'     => 'validate-email',
			'value'		=> Mage::getStoreConfig('trans_email/ident_general/email')
		));
		
		$fieldsetCompany->addField('account[website]', 'text', array(
			'name'      => 'account[website]',
			'label'     => Mage::helper('pakipoint')->__('Company Website'),
			'title'     => Mage::helper('pakipoint')->__('Company Website'),
			'required'  => true,
			'class'     => 'validate-url',
			'value'		=> str_replace('index.php/', '', Mage::getBaseUrl())
		));
		
		
		// USER FIELDSET
		$fieldsetUser = $form->addFieldset('user_fieldset', array(
			'legend' => Mage::helper('pakipoint')->__('Pakipoint Account Information'),
			'class' => 'fieldset'
		));
		
		$host = Mage::getModel('core/url')->parseUrl(Mage::getBaseUrl())->getHost();
		$username = str_replace('.', '_', $host);
		
		$fieldsetUser->addField('user[username]', 'text', array(
			'name'      => 'user[username]',
			'label'     => Mage::helper('pakipoint')->__('Username'),
			'title'     => Mage::helper('pakipoint')->__('Username'),
			'required'  => true,
			'class'     => 'validate-identifier',
			'value'		=> $username,//.'_'.Mage::helper('core')->getRandomString(4, '1234567890')
		));
		
		$fieldsetUser->addField('user[email]', 'text', array(
			'name'      => 'user[email]',
			'label'     => Mage::helper('pakipoint')->__('Email'),
			'title'     => Mage::helper('pakipoint')->__('Email'),
			'required'  => true,
			'class'     => 'validate-email',
			'value'		=> Mage::getStoreConfig('trans_email/ident_general/email')
		));
		
		$fieldsetUser->addField('user[first_name]', 'text', array(
			'name'      => 'user[first_name]',
			'label'     => Mage::helper('pakipoint')->__('First Name'),
			'title'     => Mage::helper('pakipoint')->__('First Name'),
			'required'  => false,
		));
		
		$fieldsetUser->addField('user[last_name]', 'text', array(
			'name'      => 'user[last_name]',
			'label'     => Mage::helper('pakipoint')->__('Last Name'),
			'title'     => Mage::helper('pakipoint')->__('Last Name'),
			'required'  => false,
		));
		
		$form->setUseContainer(true);
		$this->setForm($form);
		
		return parent::_prepareForm();
    }
    
    
    protected function _initFormValues()
    {
		$formData = Mage::getSingleton('adminhtml/session')->getData('pakipoint_registration_data');
		$formattedData = array();
		foreach($formData as $groupName => $subValues){
			if(!in_array($groupName, array('account', 'user'))){
				continue;
			}
			
			foreach($subValues as $name => $value){
				$formattedData[$groupName.'['.$name.']'] = $value;
			}
		}
		
		$this->getForm()->addValues($formattedData);
    }
}