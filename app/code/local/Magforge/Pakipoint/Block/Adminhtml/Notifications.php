<?php

class Magforge_Pakipoint_Block_Adminhtml_Notifications extends Mage_Adminhtml_Block_Template
{
	public function needShowRegistrationLink()
	{
		$mode = Mage::getStoreConfig('carriers/pakipoint/api_endpoint_type');
		if(!Mage::getStoreConfigFlag('carriers/pakipoint/registered_'.$mode)){
			return true;
		}
		
		return false;
	}
	
	
	public function getRegistrationUrl()
	{
		return Mage::helper('adminhtml')->getUrl('adminhtml/adminhtml_registration/index');
	}
}