<?php

class Magforge_Pakipoint_Block_Adminhtml_System_Config_Form_Field_PakipointLogin extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
		$config = Mage::getStoreConfig('carriers/pakipoint');
		
		if(
			!isset($config['api_endpoint_type']) 
			|| $config['api_endpoint_type'] === ''
			|| !isset($config[$config['api_endpoint_type'].'_auth_token']) 
			|| $config[$config['api_endpoint_type'].'_auth_token'] == ''
		){
			return '<span style="color:grey">'.Mage::helper('pakipoint')->__('Please set Authorization Token to see login link').'</span>';
		}
		
		$mode = Mage::getStoreConfig('carriers/pakipoint/api_endpoint_type');
		$button = $this->getLayout()->createBlock('adminhtml/widget_button', 'login_button');
		$button
			->setData(array(
				'label'	=> Mage::helper('pakipoint')->__('Go To Pakipoint '.strtoupper($mode).' Admin'),
				'onclick' => 'window.open(\''.Mage::helper('adminhtml')->getUrl('adminhtml/adminhtml_registration/pakipointlogin').'\')',
			));
		
		return $button->toHtml();
    }
}