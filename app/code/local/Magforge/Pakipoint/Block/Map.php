<?php

class Magforge_Pakipoint_Block_Map extends Mage_Core_Block_Template
{
	public function getApiKey()
	{
		return Mage::getStoreConfig('carriers/pakipoint/maps_api_key');
	}
	
	
	public function getRecipientGeoPosition()
	{
		return Mage::getSingleton('checkout/session')->getData('pakipoint_recipient_geoposition');
	}
	
	
	public function canShowMap()
	{
		if(
			Mage::getStoreConfigFlag('carriers/pakipoint/maps_show_results_on_map')
			&& $this->getRecipientGeoPosition() 
			&& $this->getMarkerData()
		){
			return true;
		}
		
		return false;
	}
	
	
	public function getMarkerData()
	{
		//$data = Mage::getSingleton('checkout/session')->getData('pakipoint_destinations_data');
		$data = Mage::registry('pakipoint_destinations_data');
		if($data && count($data)){
			return $data;
		}
		
		return false;
	}
	
	
	public function getSelectedShippingMethod()
	{
		$quote = Mage::getSingleton('checkout/session')->getQuote();
		if($shipMethod = $quote->getShippingAddress()->getShippingMethod()){
			return $shipMethod;
		}
		
		return false;
	}
}