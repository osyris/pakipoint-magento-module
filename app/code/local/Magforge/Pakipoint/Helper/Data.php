<?php

class Magforge_Pakipoint_Helper_Data extends Mage_Core_Helper_Abstract
{
	protected $_startTime = null;
	
	protected $_lastStepTime = null;
	
	
	/**
	 * Public Logger Function
	*/
	public function log($str, $header = false)
	{
		$headerStr = str_repeat('=', 45);
		$header ? $this->_log($headerStr) : '';
		$this->_log($str);
		$header ? $this->_log($headerStr) : '';
	}
	
	
	/**
	 * Time Logger Function
	*/
	public function logTime($str)
	{
		$current = floatval(microtime(true));
		if($this->_startTime === null){
			$this->_startTime = floatval(microtime(true));
		}
		
		if($this->_lastStepTime === null){
			$this->_lastStepTime = floatval(microtime(true));
		}
		
		$fromLastStep = round($current - $this->_lastStepTime, 4);
		$total = round($current - $this->_startTime, 4);
		
		$this->log($str.': '.$fromLastStep.'s / '.$total.'s');
		
		$this->_lastStepTime = floatval(microtime(true));
	}
	
	
	/**
	 * Logger Function
	*/
	protected function _log($str)
	{
		if(!Mage::getStoreConfigFlag('carriers/pakipoint/log_data')){
			return;
		}
		
		$fileName = 'pakipoint.log';
		if(Mage::getStoreConfig('carriers/pakipoint/log_file_name')){
			$fileName = Mage::getStoreConfig('carriers/pakipoint/log_file_name');
		}
		
		Mage::log($str, null, $fileName);
	}
	
	
	/**
	 * Generate Request Params from Request Object
	*/
    public function getRequestParams($request)
    {
		$this->log('Preparing request prameters');
		$quote = $this->getSess()->getQuote();
		$shippAddr = $quote->getShippingAddress();
		
		// prepare customer name
		$customerName = array();
		foreach(array('firstname','middlename','lastnam') as $namePart){
			if($shippAddr->getData($namePart)){
				$customerName[] = $shippAddr->getData($namePart);
			}
		}
		
		$street = $shippAddr->getStreet();
		
		$requestParams = array(
			'weight' => number_format( (float) $request->getPackageWeight(), 3, '.', ''),
			'order_total' => $request->getBaseSubtotalInclTax(),
			'recipient' => array(
				'name' => count($customerName) ? implode(' ', $customerName) : '',
				'company' => $shippAddr->getCompany() ? $shippAddr->getCompany() : '',
				'phone' => $shippAddr->getTelephone() ? $shippAddr->getTelephone() : '',
				'email' => $shippAddr->getEmail() ? $shippAddr->getEmail() : '',
				'street1' => $street[0] ? $street[0] : '',
				'city' => $shippAddr->getCity() ? $shippAddr->getCity() : '',
				'state' => $shippAddr->getRegion() ? $shippAddr->getRegion() : '',
				'postal_code' => $shippAddr->getPostcode() ? $shippAddr->getPostcode() : '',
				'country' => $shippAddr->getCountryId(),
			)
		);
		
		if(isset($street[1])){
			$requestParams['recipient']['street2'] = $street[1] ? $street[1] : '';
		}
		
		$this->log($requestParams);
		
		$this->addCoordinatesToParams($requestParams);
		
		return $requestParams;
    }
    
    
	/**
	 * Geocode Address and Add Destination Coordinates to Request Params
	*/
    public function addCoordinatesToParams(&$requestParams)
    {
		$this->log('Geocoding address and adding coordinates to request parameters');
		
		if(!Mage::getStoreConfigFlag('carriers/pakipoint_pup/active')){
			$this->log('"Pakipoint PUP" shipping method is not enabled, no need to geocode');
			return;
		}
		
		$data = $requestParams['recipient'];
		$allowedCountries = explode(',', Mage::getStoreConfig('carriers/pakipoint/maps_geocode_country'));
		if(!in_array($requestParams['recipient']['country'], $allowedCountries)){
			$this->log('... geocoding is not allowed in module settings for '.Mage::app()->getLocale()->getCountryTranslation($data['country']));
			return;
		}
		
		// skip geocoding if required parameters are missing
		$required = array('street1', 'postal_code', 'country');
		foreach($required as $paramName){
			if(!$data[$paramName]){
				$this->log('... "'.$paramName.'" is not set, skipping geocoding');
				return;
			}
		}
		
		$addr = $data['street1'];
		$addr .= ', '.$data['postal_code'];
		if($data['city']){
			$addr .= ' '.$data['city'];
		}
		if($data['state']){
			$addr .= ', '.$data['state'];
		}
	    
		$addr .= ', '.Mage::app()->getLocale()->getCountryTranslation($data['country']);
		
		$geoPosition = false;
		$cacheKey = md5(serialize(array($addr)));
		$cachedData = Mage::helper('pakipoint')->loadCache($cacheKey);
		if($cachedData){
			$this->log('... loaded from cache');
			$geoPosition = unserialize($cachedData);
			
		}else{	
			try{
				$this->log('... requesting coordinates from Google for address: "'.$addr.'"');
				$mapsApiUrl = 'https://maps.googleapis.com/maps/api/geocode/json';
				$client = (new Zend_Http_Client($mapsApiUrl))
					->setParameterGet('address', $addr)
					->setParameterGet('key', Mage::getStoreConfig('carriers/pakipoint/maps_api_key'))
					->request()
					;
				
				if($client->getStatus() !== 200){
					$this->log('ERROR: Got HTTP Status '.$client->getStatus().', skipping geocoding');
				}
				
				$response = json_decode($client->getBody());		
				if($response && $response->status !== 'OK'){
					$this->log('ERROR: Got Geocoding Status "'.$response->status.'", skipping geocoding');
				}
				
				if(count($response->results)){
					if(isset($response->results[0]->geometry)){
						
						$geoPosition = $response->results[0]->geometry->location;
						
						Mage::helper('pakipoint')->saveCache(serialize($geoPosition), $cacheKey);
						
						$this->log('SUCCESS. Address coordinates are:');
						$this->log($geoPosition);
					}
				}
			
			}catch(Exception $e){
				$this->log('ERROR: Got Exception "'.$e->getMessage().'" when geocoding address');
				Mage::logException($e);
			}
		}
		
		if($geoPosition){
			$requestParams['recipient']['location'] = array(
				'type' => 'Point',
				'coordinates' => array(
					$geoPosition->lng,
					$geoPosition->lat
				)
			);
		}
		
		$this->getSess()->setData('pakipoint_recipient_geoposition', $geoPosition);
		
		Mage::helper('pakipoint')->logTime('geocoding done');
	}
	
	
	public function getRegistrationFieldsMap()
	{
		$h = Mage::helper('pakipoint');
		$fields = array(
			'account' => array(
				'label' => $h->__('Company Information'),
				'fields' => array(
					'name' => array(
						'label' => $h->__('Account Name')
					),
					'company' => array(
						'label' => $h->__('Company Name'),
					),
					'registration_number' => array(
						'label' => $h->__('Registration Number'),
					),
					'vat_number' => array(
						'label' => $h->__('VAT Number'),
					),
					'address' => array(
						'label' => $h->__('Registered Address'),
					),
					'city' => array(
						'label' => $h->__('Registered City'),
					),
					'postal_code' => array(
						'label' => $h->__('Registered Zip/Postal Code'),
					),
					'country' => array(
						'label' => $h->__('Registered Country'),
					),
					'phone' => array(
						'label' => $h->__('Company Phone Number'),
					),
					'email' => array(
						'label' => $h->__('Company Email Address'),
					),
					'website' => array(
						'label' => $h->__('Company Website'),
					),
				)
			),
			'user' => array(
				'label' => $h->__('Pakipoint Account Information'),
				'fields' => array(
					'username' => array(
						'label' => $h->__('Username'),
					),
					'email' => array(
						'label' => $h->__('Email'),
					),
					'first_name' => array(
						'label' => $h->__('First Name'),
					),
					'last_name' => array(
						'label' => $h->__('Last Name'),
					),
				)	
			)
		);
		
		return $fields;
	}
	
	
	public function getFormattedRegistrationErrorMessages($group, $fields)
	{
		$result = array();
		$map = $this->getRegistrationFieldsMap();
		
		$groupName = ucfirst($group).' / ';
		if(isset($map[$group])){
			$groupName = $map[$group]['label'].' / ';
		}
		
		foreach((array) $fields as $name => $errors){
			$fieldName = ucfirst($name);
			if(isset($map[$group]['fields'][$name])){
				$fieldName = $map[$group]['fields'][$name]['label'];
			}
			
			foreach($errors as $msg){
				$result[] = $groupName.$fieldName.' - '.$msg;
			}
		}
		
		return $result;
	}
	
	
//	public function getFormattedRegistrationErrorXXX($error)
//	{
//		if(!is_array($error)){
//			return $error;
//		}
//		
//		Magf::pr($error); die();
//		
//		$map = $this->getRegistrationFieldsMap();
//		$outLabel = array();
//		$outMsg = '';
//		while(is_array($error)){
//			foreach($error as $k => $v){
//				if(!isset($map[$k])){
//					$outMsg = $v;
//					$error = false;
//				}
//				
//				$out[] = $map[$k]['label'];
//				$error = (array) $v;
//				$map = $map[$k]['fields'];
//			}
//		}
//		
//		if(count($outLabel)){
//			return '"'.implode(':', $outLabel).'" '.$outMsg;
//		}
//		
//		return $outMsg;
//	}
	

	/**
	 * Get Price Key Based on Magento Config
	*/
	public function getPriceKey()
	{
		$priceKey = 'price_no_vat';
		if(Mage::getStoreConfigFlag('tax/calculation/shipping_includes_tax')){
			$priceKey = 'price_with_vat';
		}
		
		return $priceKey;
	}
	
	
	/**
	 * Get Checkout Session Shorthand
	*/
    public function getSess()
    {
	    return Mage::getSingleton('checkout/session');
    }
	
	
	/**
	 * Cache Loader Function
	*/
	public function loadCache($cacheKey)
	{
		if($cachedData = Mage::app()->loadCache($cacheKey)){
			return $cachedData;
		}
		
		return false;
	}
	
	
	/**
	 * Cache Saver Function
	*/
	public function saveCache($data, $cacheKey)
	{
		// disabled this check on 11.09.2017 by Kostja, as when BLOCK_CACHE is disabled
		// rates are requested on every Cart and Checkout page load, which results
		// in poor UX
		
//		if(!Mage::app()->getCacheInstance()->canUse('block_html')){
//			return;
//		}
		
		Mage::app()->saveCache(
			$data,
			$cacheKey,
			array('BLOCK_HTML'),
			600
		);
		
		return;
	}
}