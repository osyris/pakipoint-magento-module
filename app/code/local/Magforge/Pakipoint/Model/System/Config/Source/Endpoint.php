<?php

class Magforge_Pakipoint_Model_System_Config_Source_Endpoint
{
    protected $_options;

    public function toOptionArray($isMultiselect=false)
    {
        if (!$this->_options) {
            
			$this->_options = array(
				array(
					'value' => 'test',
					'label' => 'Test System'
				),	
				array(
					'value' => 'live',
					'label' => 'Live System'
				),		
			);
			
//			if(!$isMultiselect){
//				array_unshift($this->_options, array('value'=>'', 'label'=> Mage::helper('adminhtml')->__('--Please Select--')));
//			}
		}

        return $this->_options;
    }
}
