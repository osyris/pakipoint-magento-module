<?php
	
class Magforge_Pakipoint_Model_Observer
{
	/**
	 * Observer to Inject Map Block To Shipping Options
	*/
	public function addMapToShippingOptionsInCheckout($observer)
	{
		if(!Mage::getStoreConfigFlag('carriers/pakipoint/maps_show_results_on_map')){
			return;
		}
		
		$block = $observer->getEvent()->getBlock();
		
		if(!$block instanceof Mage_Checkout_Block_Onepage_Shipping_Method_Available){
			return;
		}
		
		$html = $observer->getEvent()->getTransport()->getHtml();
		$mapBlock = $block->getLayout()->createBlock('pakipoint/map', 'pakipoint.checkout.map')
			->setTemplate('magforge/pakipoint/map.phtml')
			;
			
		$html = $mapBlock->toHtml().$html;
		
		$observer->getEvent()->getTransport()->setHtml($html);
	}
}