<?php

class Magforge_Pakipoint_Model_Api_Client
{
	private $_config = array();
	
	private $_client = null;
	
	private $_params = array();
	
	private $_cacheParams = array();
	
	private $_error = null;
	
	private $_response = array();
	
	
	/**
	 * Constructor
	*/
	public function __construct()
	{	
		$config = Mage::getStoreConfig('carriers/pakipoint');
		
		// commented out on 08.06.17 by Konstantin, as these checks produced an error when registering account with Pakipoint
//		if(!isset($config['api_endpoint_type']) || $config['api_endpoint_type'] === ''){
//			throw new Exception('API Endpoint is not set in module configuration');
//		}
//		if(!isset($config[$config['api_endpoint_type'].'_merchant_code']) || $config[$config['api_endpoint_type'].'_merchant_code'] == ''){
//			throw new Exception('Merchant Code is not set in module configuration');
//		}
//		if(!isset($config[$config['api_endpoint_type'].'_auth_token']) || $config[$config['api_endpoint_type'].'_auth_token'] == ''){
//			throw new Exception('Autorization Token is not set in module configuration');
//		}
		
		$this->log($config);
		
		$this->_config = $config;
	}
	
	
	/**
	 * HTTP Client initialization
	*/
	public function initClient($apiType, $requestMethod)
	{
		$endpointType = $this->_config['api_endpoint_type'];
		$serviceUrl = $this->_config[$endpointType.'_endpoint'].$apiType.'/';
		
		$this->_client = (new Zend_Http_Client())
			->setUri($serviceUrl)
			->setHeaders('Authorization', 'Token '.$this->_config[$endpointType.'_auth_token'])
			->setHeaders('Accept-Language', substr(Mage::app()->getLocale()->getLocaleCode(), 0, 2))
			->setMethod($requestMethod)
			;
		
		$this->_error = null;
		$this->_params = array();
		$this->_cacheParams = array();
	}
	
	
	/**
	 * Class internal config getter
	*/
	public function getConfig($paramName)
	{
		if(!isset($this->_config[$paramName])){
			return null;
		}
		
		return $this->_config[$paramName];
	}
	
	
	/**
	 * Class internal config setter
	*/
	public function setConfig($paramName, $paramValue)
	{
		$this->_config[$paramName] = $paramValue;
		
		return $this;
	}
	
	
	/**
	 * Request result error getter
	*/
	public function getError()
	{
		return $this->_error;
	}
	
	
	/**
	 * Request result getter
	*/
	public function getResponse()
	{
		return json_decode($this->_response);
	}
	
	
	/**
	 * Request parameter setter
	*/
	public function setParameter($paramName, $paramValue)
	{
		$this->_params[$paramName] = $paramValue;

		return $this;
	}
	
	
	/**
	 * Request parameter getter
	*/
	public function getParameter($paramName)
	{
		return isset($this->_params[$paramName]) ? $this->_params[$paramName] : null;
	}
	
	
	/**
	 * Request Price Quotes from Pakipoint
	*/
	public function getPriceQuotes($params = array())
	{
		$this->log('Requesting "Price Offers"');
		
		$this->initClient('price_queries', 'POST');
		$this->_params = array(
			'merchant_code' => $this->_config[$this->_config['api_endpoint_type'].'_merchant_code'],
			'customer_type' => 'private_person',
		);
		
		foreach($params as $paramName => $paramData){
			$this->setParameter($paramName, $paramData);
		}
		
		$cacheKey = $this->getCacheKey();
		$cachedData = Mage::helper('pakipoint')->loadCache($cacheKey);
		if($cachedData){
			$this->log('... loaded from cache');
			return unserialize($cachedData);
			
		}else{
			try{
				$this->request();
				if(!isset($this->getResponse()->price_quotes)){
					throw new Exception('Response received, but "price_quotes" element is not present');
				}
				
				Mage::helper('pakipoint')->logTime('price offers received');
				$quotes = $this->getResponse()->price_quotes;
				
				$needRequestDestinations = false;
				foreach($quotes as $quote){
					if($quote->destination_required){
						$needRequestDestinations = true;
						break;
					}
				}
				
				if($needRequestDestinations){
					$destinations = $this->getDestinationsByReference($this->getResponse()->reference);
				
					Mage::helper('pakipoint')->logTime('destinations received');
				
					foreach($quotes as $quote){
						if($quote->destination_required && isset($params['recipient']['location'])){
							if(!isset($quote->destinations)){
								$quote->destinations = array();
							}
							
							$method = $quote->delivery_method;
							$service = $quote->delivery_service;
							foreach($destinations as $destination){
								if(
									$destination->shipping_method == $method 
									&& $destination->shipping_service == $service
								){
									$quote->destinations[] = $destination;
								}
							}
						}
					}
				}
				
				$this->log('Received following Price Offers:');
				$this->log($quotes);
				
				Mage::helper('pakipoint')->saveCache(serialize($quotes), $cacheKey);
				
				return $quotes;
				
			}catch(Exception $e){
				Mage::logException($e);
				$this->_error = $e->getMessage();
				$this->log('ERROR: '.$e->getMessage());
				
				return array();
			}
		}
	}
	
	
	/**
	 * Request Destinations (PUPs or delivery points) from Pakipoint
	*/
	public function getDestinationsByReference($refNum)
	{
		$this->log('Requesting "Destinations" for '.$refNum);
		$this->initClient('destinations', 'GET');
		$this->_params = array();
		$params = array('price_query_reference' => $refNum);
		$this->_cacheParams = $params;
		$this->_client->setParameterGet($params);
		
		try{
			$this->request();
			return $this->getResponse();
			
		}catch(Exception $e){
			Mage::logException($e);
			$this->_error = $e->getMessage();
			$this->log('ERROR: '.$e->getMessage());
			
			return array();
		}
	}
	
	
	/**
	 * Register NEW Account with pakipoint
	*/
	public function registerAccount($params = array())
	{
		$this->log('REGISTERING NEW ACCOUNT', true);
		$this->initClient('registrations', 'POST');
		$this->_params = $params;
		$this->_client->setParameterPost($params);
		
		try{
			$this->request();
			return $this->getResponse();
			
		}catch(Exception $e){
			Mage::logException($e);
			$this->_error = $e->getMessage();
			$this->log('ERROR: '.$e->getMessage());
			
			return array();
		}
	}
	
	
	/**
	 * Send Magento SOAP API Credentials to Pakipoint. Pakipoint uses them to access order information later on.
	*/
	public function registerMagentoApiCredentials($params = array())
	{
		$this->log('REGISTERING MAGENTO API CREDENTIALS', true);
		$this->initClient('access_keys', 'POST');
		$this->_params = $params;
		$this->_client->setParameterPost($params);
		
		try{
			$this->request();
			return $this->getResponse();
			
		}catch(Exception $e){
			Mage::logException($e);
			$this->_error = $e->getMessage();
			$this->log('ERROR: '.$e->getMessage());
			
			return array();
		}
	}
	
	
	/**
	 * Request login link from Pakipoint
	*/
	public function requestLoginLink()
	{
		$this->log('REQUESTING LOGIN LINK', true);
		$this->initClient('login_links', 'POST');
		$this->_params = array();
		$this->_client->setParameterPost(array());
		
		try{
			$this->request();
			return $this->getResponse();
			
		}catch(Exception $e){
			Mage::logException($e);
			$this->_error = $e->getMessage();
			$this->log('ERROR: '.$e->getMessage());
			
			return array();
		}
	}
	
	
	/**
	 * Request Handler
	*/
	public function request()
	{	
		$this->log('... sending request to Pakipoint API');
		
		if(count($this->_params)){
			$this->log(json_encode($this->_params));
			$this->_client->setRawData(json_encode($this->_params), 'application/json');
		}
		
//		$cachedData = Mage::helper('pakipoint')->loadCache($this->getCacheKey());
//		if($cachedData){
//			$this->log('... loaded from cache using key: '.$this->getCacheKey());
//			$this->log($cachedData);
//			$this->_response = $cachedData;
//			return;
//		}
		
		$result = $this->_client->request();
		$this->_response = $result->getBody();
		
		switch($result->getStatus()){

			// good response
			case 200:
			case 201:
				if($this->getResponse() === null){
					throw new Exception('API response body in not valid JSON');
				}

//				Mage::helper('pakipoint')->saveCache($this->_response, $this->getCacheKey());
				$this->log('SUCCESS');
				break;
				
			// error in provided data, except token
			case 400:
				if($this->getResponse()){
					throw new Exception($result->getBody());
					
				}else{
					throw new Exception($result->getMessage());
				}
				
			// unauthorized, wrong token
			case 401:
				throw new Exception('Invalid Authorization Token');
				
			// wrong url
			case 404:
				throw new Exception('Got "404:'.$result->getMessage().'" when sending request to "'.$this->_client->getUri().'"');
			
			// all other cases
			default: 
				throw new Exception($result->asString());
		}
	}
	
	
	/**
	 * Cache Key Getter. Used by other request functions to cache their responses.
	*/
	public function getCacheKey()
	{		
		$cacheKey = array(
			$this->_params,
			$this->_cacheParams,
			$this->_client->getUri(true),
		);
		
		return md5(serialize($cacheKey));
	}
    
    
	/**
	 * Logger
	*/
	public function log($str, $header = false)
	{
		return Mage::helper('pakipoint')->log($str, $header);
	}
}