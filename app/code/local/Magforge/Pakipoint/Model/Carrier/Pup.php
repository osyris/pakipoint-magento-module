<?php

class Magforge_Pakipoint_Model_Carrier_Pup extends Magforge_Pakipoint_Model_Carrier_Base implements Mage_Shipping_Model_Carrier_Interface
{
	/**
	 * Shipping Method Code
	*/
	protected $_code = 'pakipoint_pup';
	
	/**
	 * Types of Services to Process
	*/
	protected $_deliveryServiceTypes = array('pickup_point', 'parcel_machine');
	
	/**
	 * Map Data Container
	*/
	protected $_destinationsData = array();
	
	
	/**
	 * Prepare PUP Rates
	*/
	public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
		$this->log('PREPARING RATES PUP', true);
		
		if (!$this->getConfigFlag('active')) {
			$this->log('not active, existing...');
			return false;
		}
		
		Mage::helper('pakipoint')->logTime('start');
		
		$result = Mage::getModel('pakipoint/shipping_rate_result');
		$priceKey = Mage::helper('pakipoint')->getPriceKey();
		$offers = array();
		if(Mage::registry('pakipoint_offers')){
			$offers = Mage::registry('pakipoint_offers');
		}
		
		// add shipping options to result
		foreach($offers as $offer){
			
			if(!in_array($offer->delivery_service, $this->_deliveryServiceTypes)){
				continue;
			}
			
			if($offer->destination_required && isset($offer->destinations) && count($offer->destinations)){
				foreach($offer->destinations as $destination){
					$title = $this->getMethodTitle($destination->display_name);
					$method = Mage::getModel('shipping/rate_result_method')
		    			->setCarrier($this->_code)
		    			->setCarrierTitle($this->getConfigData('title'))
		    			->setMethod($offer->reference.'_'.$destination->id)
		    			->setMethodTitle($title)
						->setPrice($offer->$priceKey)
						->setCost($offer->$priceKey)
		    			;
		    		
		    		$destination->price_offer = $offer;
		    		$this->processDestinationForMap($destination);
		    		
					$result->append($method);
				}
				
			}
		}
		
		Mage::register('pakipoint_destinations_data', $this->_destinationsData, true);
		
		Mage::helper('pakipoint')->logTime('rates prepared');

		return $result;
    }
    
    
	/**
	 * Add Geolocated Delivery Destination to Map Data
	*/
    protected function processDestinationForMap($destination)
    {
	    $item = new Varien_Object(array(
			'id' => $this->_code.'_'.$destination->price_offer->reference.'_'.$destination->id,
		    'title' => $destination->name,
		    'type' => $destination->shipping_method,
	    ));
	    
	    if(isset($destination->location->coordinates)){
		    $item->setLng($destination->location->coordinates[0]);
		    $item->setLat($destination->location->coordinates[1]);
	    }
	    
	    $this->_destinationsData[] = $item;
    }
}