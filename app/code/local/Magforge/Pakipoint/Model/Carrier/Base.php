<?php

class Magforge_Pakipoint_Model_Carrier_Base extends Mage_Shipping_Model_Carrier_Abstract implements Mage_Shipping_Model_Carrier_Interface
{
	/**
	 * Shipping Method Code
	*/
	protected $_code = 'pakipoint';
	
	protected $_isFixed = true;
	
	
	/**
	 * Request and Prepare Rates
	*/
	public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
		$this->log('COLLECTING RATES BASE', true);
		
		if (!$this->getConfigFlag('active')) {
			$this->log('not active, existing...');
			return false;
		}
		
		Mage::helper('pakipoint')->logTime('start');
		
		$result = Mage::getModel('pakipoint/shipping_rate_result');
		$client = Mage::getSingleton('pakipoint/api_client');
		
		if(!Mage::registry('pakipoint_offers') || Mage::registry('pakipoint_force_refresh')){
			$offers = $client->getPriceQuotes(Mage::helper('pakipoint')->getRequestParams($request));
			Mage::register('pakipoint_offers', $offers, true);
		}
		
		// add error to result
		if(Mage::getStoreConfigFlag('carriers/pakipoint/debug') && $errorStr = $client->getError()){
			$error = Mage::getModel('shipping/rate_result_error');
			$error->setCarrier($this->_code);
			$error->setCarrierTitle('Pakipoint');
			$error->setErrorMessage($errorStr);
			$result->append($error);
		}
		
		Mage::helper('pakipoint')->logTime('offers collected');
		
		return $result;
    }
    
    
	/**
	 * Get Final Method Title
	*/
    protected function getMethodTitle($name)
    {
		if(Mage::getStoreConfigFlag('carriers/pakipoint/truncate_method_name')){
			$length = Mage::getStoreConfig('carriers/pakipoint/truncate_method_name_to');
			if($length && $length > 1){
				$name = Mage::helper('core/string')->truncate($name, $length);
			}
		}
		
		return $name;
    }
	
	
	/**
	 * Get Checkout Session Shorthand
	*/
	public function getSess()
	{
		return Mage::getSingleton('checkout/session');
	}
	
	
	/**
	 * Get allowed shipping methods
	*/
	public function getAllowedMethods()
	{
		$result = array();
		return $result;
	}
	
	
	/**
	 * Logger
	*/
	public function log($str, $header = false)
	{
		return Mage::helper('pakipoint')->log($str, $header);
	}
}