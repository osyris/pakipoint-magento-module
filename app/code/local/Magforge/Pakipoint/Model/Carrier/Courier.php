<?php

class Magforge_Pakipoint_Model_Carrier_Courier extends Magforge_Pakipoint_Model_Carrier_Base implements Mage_Shipping_Model_Carrier_Interface
{
	/**
	 * Shipping Method Code
	*/
	protected $_code = 'pakipoint_courier';
	
	/**
	 * Types of Services to Process
	*/
	protected $_deliveryServiceTypes = array('courier_standard');
	
	
	/**
	 * Prepare Courier Rates
	*/
	public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
		$this->log('PREPARING RATES COURIER', true);
		
		if (!$this->getConfigFlag('active')) {
			$this->log('not active, existing...');
			return false;
		}
		
		Mage::helper('pakipoint')->logTime('strat');
		
		$result = Mage::getModel('pakipoint/shipping_rate_result');		
		$priceKey = Mage::helper('pakipoint')->getPriceKey();
		$offers = array();
		if(Mage::registry('pakipoint_offers')){
			$offers = Mage::registry('pakipoint_offers');
		}
		
		// add shipping options to result
		foreach($offers as $offer){
			
			if(!in_array($offer->delivery_service, $this->_deliveryServiceTypes)){
				continue;
			}
			
			$title = $this->getMethodTitle($offer->display_name);
			$method = Mage::getModel('shipping/rate_result_method')
				->setCarrier($this->_code)
		    	->setCarrierTitle($this->getConfigData('title'))
	    		->setMethod($offer->reference)
	    		->setMethodTitle($title)
				->setPrice($offer->$priceKey)
				->setCost($offer->$priceKey)
	    		;
	    	
			$result->append($method);
		}
		
		Mage::helper('pakipoint')->logTime('rates prepared');

        return $result;
    }
}